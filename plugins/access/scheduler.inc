<?php

/**
 * @file
 * Ctools access plugin for scheduling the display of Panel panes.
 */

$plugin = array(
  'title' => t('Date time schedule'),
  'description' => t('Display or hide this pane if the current date falls within 2 date time ranges.'),
  'callback' => 'panel_pane_scheduler_ctools_access_check',
  'settings form' => 'panel_pane_scheduler_ctools_access_settings',
  'summary' => 'panel_pane_scheduler_ctools_access_summary',
);

/**
 * Settings form for pane date time schedule.
 */
function panel_pane_scheduler_ctools_access_settings($form, &$form_state, $conf) {
  // Create hours and minutes ranges as arrays for form select options.
  $hours = array_map('panel_pane_scheduler_zero_pad_low_values', range(0, 23));
  $minutes = array_map('panel_pane_scheduler_zero_pad_low_values', range(0, 59));

  $form['#validate'][] = 'panel_pane_scheduler_ctools_access_settings_validate';

  $form['settings']['from'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display from'),
    '#weight' => -4,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#attributes' => array('class' => array('container-inline')),
  );

  $form['settings']['from']['date'] = array(
    '#type' => 'date',
    '#title' => t('Date'),
    '#default_value' => $conf['from']['date'],
    '#required' => TRUE,
  );

  $form['settings']['from']['hour'] = array(
    '#type' => 'select',
    '#title' => t('Hour'),
    '#options' => $hours,
    '#default_value' => $conf['from']['hour'],
    '#required' => TRUE,
  );

  $form['settings']['from']['minute'] = array(
    '#type' => 'select',
    '#title' => t('Minute'),
    '#options' => $minutes,
    '#default_value' => $conf['from']['minute'],
    '#required' => TRUE,
  );

  $form['settings']['to'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display until'),
    '#weight' => -3,
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#attributes' => array('class' => array('container-inline')),
  );

  $form['settings']['to']['date'] = array(
    '#type' => 'date',
    '#title' => t('Date'),
    '#default_value' => $conf['to']['date'],
    '#required' => TRUE,
  );

  $form['settings']['to']['hour'] = array(
    '#type' => 'select',
    '#title' => t('Hour'),
    '#options' => $hours,
    '#default_value' => $conf['to']['hour'],
    '#required' => TRUE,
  );

  $form['settings']['to']['minute'] = array(
    '#type' => 'select',
    '#title' => t('Minute'),
    '#options' => $minutes,
    '#default_value' => $conf['to']['minute'],
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Settings form validation callback function.
 */
function panel_pane_scheduler_ctools_access_settings_validate($form, &$form_state, $conf) {
  $from_date = panel_pane_scheduler_timestamp_from_array($form_state['values']['settings']['from']);
  $to_date = panel_pane_scheduler_timestamp_from_array($form_state['values']['settings']['to']);

  // Validate that the 'Display until' date is after the 'Display from' date.
  if ($from_date > $to_date) {
    form_error($form['settings']['to']['hour']);
    form_error($form['settings']['to']['minute']);
    form_error($form['settings']['to']['date'], t("The 'Display until' date must be a date and time after the 'Display from' date."));
  }
}

/**
 * Display a summary description on pane context menu.
 */
function panel_pane_scheduler_ctools_access_summary($conf, $context) {
  return t('Visible between @from and @to', array(
    '@from' => format_date(panel_pane_scheduler_timestamp_from_array($conf['from']), 'short'),
    '@to' => format_date(panel_pane_scheduler_timestamp_from_array($conf['to']), 'short'),
  )
  );
}

/**
 * Check if the pane should be visible or not.
 */
function panel_pane_scheduler_ctools_access_check($conf, $context) {
  $from_date = panel_pane_scheduler_timestamp_from_array($conf['from']);
  $to_date = panel_pane_scheduler_timestamp_from_array($conf['to']);

  return time() > $from_date && time() < $to_date;
}

/**
 * Pad any single character or integer with a leading zero.
 *
 * @param int $value
 *   Value to pad.
 *
 * @return string
 *   Zero padded string value.
 */
function panel_pane_scheduler_zero_pad_low_values($value) {
  return str_pad($value, 2, "0", STR_PAD_LEFT);
}
