<?php

/**
 * @file
 * Ctools access plugin for the recurring display of Panel panes.
 */

$plugin = array(
  'title' => t('Date time recurring schedule'),
  'description' => t('Display or hide this pane on a date time recurring basis.'),
  'callback' => 'panel_pane_scheduler_recurring_ctools_access_check',
  'settings form' => 'panel_pane_scheduler_recurring_ctools_access_settings',
  'summary' => 'panel_pane_scheduler_recurring_ctools_access_summary',
);

/**
 * Settings form for pane date time recurring schedule.
 */
function panel_pane_scheduler_recurring_ctools_access_settings($form, &$form_state, $conf) {
  // CSS for improved form layout.
  $form['#attributes'] = array('class' => 'panel-pane-scheduler-settings-form');
  drupal_add_css(drupal_get_path('module', 'panel_pane_scheduler') . '/panel_pane_scheduler.css');

  // Add validation function to the form.
  $form['#validate'][] = 'panel_pane_scheduler_recurring_ctools_access_settings_validate';

  $form['settings']['from']['date'] = array(
    '#type' => 'date',
    '#title' => t('Display from'),
    '#description' => t('Commence the recurring schedule from this date.'),
    '#default_value' => $conf['from']['date'],
    '#required' => TRUE,
  );

  $form['settings']['duration'] = array(
    '#type' => 'fieldset',
    '#title' => t('Run the schedule'),
  );

  $form['settings']['duration']['type'] = array(
    '#type' => 'radios',
    '#options' => array(
      'forever' => t('Forever'),
      'occurrences' => t('A specific number of times'),
      'date' => t('Until a certain date'),
    ),
    '#default_value' => ($conf['duration']['type'] == '' ? 'forever' : $conf['duration']['type']),
  );

  $form['settings']['duration']['occurrences'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of times to run the schedule'),
    '#size' => 4,
    '#maxlength' => 4,
    '#default_value' => $conf['duration']['occurrences'],
    '#states' => array(
      'visible' => array(
        ':input[name="settings[duration][type]"]' => array('value' => 'occurrences'),
      ),
    ),
  );

  $form['settings']['duration']['date'] = array(
    '#type' => 'date',
    '#title' => t('Display until'),
    '#default_value' => $conf['duration']['date'],
    '#states' => array(
      'visible' => array(
        ':input[name="settings[duration][type]"]' => array('value' => 'date'),
      ),
    ),
  );

  $form['settings']['pattern'] = array(
    '#type' => 'radios',
    '#title' => t('Recurring pattern'),
    '#options' => array(
      'daily' => t('Daily'),
      'weekly' => t('Weekly'),
      'monthly' => t('Monthly'),
      'yearly' => t('Yearly'),
    ),
    '#default_value' => $conf['pattern'],
    '#required' => TRUE,
  );

  $form['settings']['daily'] = array(
    '#type' => 'fieldset',
    '#title' => t('Daily'),
    '#states' => array(
      'visible' => array(
        ':input[name="settings[pattern]"]' => array('value' => 'daily'),
      ),
    ),
  );

  $form['settings']['daily']['every'] = array(
    '#type' => 'textfield',
    '#title' => t('Every'),
    '#size' => 3,
    '#maxlength' => 3,
    '#suffix' => t('days'),
    '#default_value' => $conf['daily']['every'],
  );

  $form['settings']['weekly'] = array(
    '#type' => 'fieldset',
    '#title' => t('Weekly'),
    '#states' => array(
      'visible' => array(
        ':input[name="settings[pattern]"]' => array('value' => 'weekly'),
      ),
    ),
  );

  $form['settings']['weekly']['every'] = array(
    '#type' => 'textfield',
    '#title' => t('Every'),
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $conf['weekly']['every'],
    '#suffix' => t('weeks'),
  );

  $form['settings']['weekly']['days'] = array(
    '#title' => t('On the following days'),
    '#type' => 'checkboxes',
    '#options' => array(
      1 => t('Monday'),
      2 => t('Tuesday'),
      3 => t('Wednesday'),
      4 => t('Thursday'),
      5 => t('Friday'),
      6 => t('Saturday'),
      7 => t('Sunday'),
    ),
    '#default_value' => $conf['weekly']['days'],
  );

  $form['settings']['monthly'] = array(
    '#type' => 'fieldset',
    '#title' => t('Monthly'),
    '#states' => array(
      'visible' => array(
        ':input[name="settings[pattern]"]' => array('value' => 'monthly'),
      ),
    ),
  );

  $form['settings']['monthly']['every'] = array(
    '#type' => 'textfield',
    '#title' => t('Every'),
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $conf['monthly']['every'],
    '#suffix' => t('months'),
  );

  $form['settings']['monthly']['day'] = array(
    '#type' => 'textfield',
    '#title' => t('on the'),
    '#size' => 3,
    '#maxlength' => 3,
    '#suffix' => t('day of the month'),
    '#default_value' => $conf['monthly']['day'],
  );

  $form['settings']['yearly'] = array(
    '#type' => 'fieldset',
    '#title' => t('Yearly'),
    '#states' => array(
      'visible' => array(
        ':input[name="settings[pattern]"]' => array('value' => 'yearly'),
      ),
    ),
  );

  $form['settings']['yearly']['every'] = array(
    '#type' => 'textfield',
    '#title' => t('Every'),
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $conf['yearly']['every'],
    '#suffix' => t('years'),
  );

  $form['settings']['yearly']['day'] = array(
    '#type' => 'textfield',
    '#title' => t('on'),
    '#size' => 3,
    '#maxlength' => 3,
    '#default_value' => $conf['yearly']['day'],
  );

  $form['settings']['yearly']['month'] = array(
    '#type' => 'select',
    '#options' => array(
      1 => t('January'),
      2 => t('February'),
      3 => t('March'),
      4 => t('April'),
      5 => t('May'),
      6 => t('June'),
      7 => t('July'),
      9 => t('September'),
      10 => t('October'),
      11 => t('November'),
      12 => t('December'),
    ),
    '#default_value' => $conf['yearly']['month'],
  );

  return $form;
}

/**
 * Settings form validation callback function.
 */
function panel_pane_scheduler_recurring_ctools_access_settings_validate($form, &$form_state, $conf) {
  // Retrieve the pattern in use.
  $pattern = strval($form_state['values']['settings']['pattern']);

  // If a recur until date is set, check the from date is before the until date.
  if ($form_state['values']['settings']['duration']['type'] == 'date') {
    $from_date = panel_pane_scheduler_timestamp_from_array($form_state['values']['settings']['from']);
    $until_date = panel_pane_scheduler_timestamp_from_array($form_state['values']['settings']['duration']);

    // Validate that the 'Display until' date is after the 'Display from' date.
    if ($from_date > $until_date) {
      form_error($form['settings']['duration']['date'], t("The 'Display until' date must be a date after the 'Display from' date."));
    }
  }

  /*
  Each pattern has an 'every' field, check the value is a positive whole
  number.
   */
  if (!ctype_digit($form_state['values']['settings'][$pattern]['every'])) {
    form_error($form['settings'][$pattern]['every'], t("Every setting must be a positive whole number."));
  }

  switch ($pattern) {
    case 'monthly':
      // Ensure the day value is a positive whole number between 1 and 31.
      if (!ctype_digit($form_state['values']['settings']['monthly']['day'])) {
        form_error($form['settings']['monthly']['day'], t("Day setting must be a positive whole number."));
      }
      elseif ($form_state['values']['settings']['monthly']['day'] < 1 || $form_state['values']['settings']['monthly']['day'] > 31) {
        form_error($form['settings']['monthly']['day'], t("Day setting must be between 1 and 31."));
      }
      break;

    case 'yearly':
      /*
      Ensure the day value is a positive whole number within the 1 and 31
      range and is valid for the selected month.
       */
      if (!ctype_digit($form_state['values']['settings']['yearly']['day'])) {
        form_error($form['settings']['yearly']['day'], t("Day setting must be a positive whole number."));
      }
      elseif ($form_state['values']['settings']['yearly']['day'] < 1 || $form_state['values']['settings']['yearly']['day'] > 31) {
        form_error($form['settings']['yearly']['day'], t("Day setting must be between 1 and 31."));
      }
      elseif ($form_state['values']['settings']['yearly']['day'] > cal_days_in_month(CAL_GREGORIAN, $form_state['values']['settings']['yearly']['month'], 2016)) {
        form_error($form['settings']['yearly']['day'], t("The day setting is not valid for the selected month."));
      }
      break;
  }

  /*
  If a duration is set, retrieve a copy of the settings as we will add these
  back into the form settings after all the irrelevant duration settings
  have been removed.
   */
  if ($form_state['values']['settings']['duration']['type'] != 'forever') {
    $duration_type = strval($form_state['values']['settings']['duration']['type']);
    $temp_array_duration = $form_state['values']['settings']['duration'][$duration_type];
  }

  // Remove all settings except the settings we want to retain.
  $form_state['values']['settings'] = array_intersect_key($form_state['values']['settings'], array(
    'from' => 'from',
    'pattern' => 'pattern',
    $pattern => $pattern,
  ));

  // Add the chosen duration settings back into the form settings values.
  $form_state['values']['settings']['duration']['type'] = $duration_type;
  $form_state['values']['settings']['duration'][$duration_type] = $temp_array_duration;
}

/**
 * Display a summary description on pane form-checkboxext menu.
 */
function panel_pane_scheduler_recurring_ctools_access_summary($conf, $context) {

  // Display a panels summary for each pattern.
  switch ($conf['pattern']) {
    case 'daily':
      return t('Recurring every @days day(s)', array('@days' => $conf['daily']['every']));

    case 'weekly':
      return t('Recurring every @weeks week(s)', array('@weeks' => $conf['weekly']['every']));

    case 'monthly':
      return t('Recurring every @months month(s) on the @day', array(
        '@months' => $conf['monthly']['every'],
        '@day' => $conf['monthly']['day'],
      ));

    case 'yearly':
      return t('Recurring every @years year(s) on the @day @month', array(
        '@years' => $conf['yearly']['every'],
        '@day' => $conf['yearly']['day'],
        '@month' => jdmonthname($conf['yearly']['month'], 1),
      ));

  }
}

/**
 * Check if the pane should be visible or not.
 */
function panel_pane_scheduler_recurring_ctools_access_check($conf, $context) {
  // Check if the from date has passed.
  $from_timestamp = panel_pane_scheduler_timestamp_from_array($conf['from']);

  if (time() < $from_timestamp) {
    return FALSE;
  }

  // Check if the until date duration has passed if set as the duration option.
  if (isset($conf['duration']['date'])) {
    $until_timestamp = panel_pane_scheduler_timestamp_from_array($conf['duration']);

    if (time() > $until_timestamp) {
      return FALSE;
    }
  }

  // Hand off the pattern type to the relevant function.
  switch ($conf['pattern']) {
    case 'daily':
      return panel_pane_scheduler_recurring_daily($conf, $context);

    case 'weekly':
      return panel_pane_scheduler_recurring_weekly($conf, $context);

    case 'monthly':
      return panel_pane_scheduler_recurring_monthly($conf, $context);

    case 'yearly':
      return panel_pane_scheduler_recurring_yearly($conf, $context);
  }
}

/**
 * Pane access check for daily pattern.
 */
function panel_pane_scheduler_recurring_daily($conf, $context) {
  $from_timestamp = panel_pane_scheduler_timestamp_from_array($conf['from']);
  $interval = $conf['daily']['every'];
  $difference = floor((time() - $from_timestamp) / 60 / 60 / 24);

  // If occurrences is set, check if the number has elapsed.
  if (panel_pane_scheduler_recurring_duration_occurence_expired($difference, $conf)) {
    return FALSE;
  }

  // Return if difference is divisable by interval without a remainder.
  return ($difference % $interval == 0);
}

/**
 * Pane access check for weekly pattern.
 */
function panel_pane_scheduler_recurring_weekly($conf, $context) {
  $from_timestamp = panel_pane_scheduler_timestamp_from_array($conf['from']);
  $interval = $conf['weekly']['every'];
  $from_week = date("W", $from_timestamp);
  $current_week = date("W", time());
  $difference = $current_week - $from_week;

  // If occurrences is set, check if the number has elapsed.
  if (panel_pane_scheduler_recurring_duration_occurence_expired($difference, $conf)) {
    return FALSE;
  }

  if ($difference % $interval == 0) {
    $days_recurring = $conf['weekly']['days'];
    $current_day = date("N", time());
    return in_array($current_day, $days_recurring);
  }
}

/**
 * Pane access check for monthly pattern.
 */
function panel_pane_scheduler_recurring_monthly($conf, $context) {
  $from_timestamp = panel_pane_scheduler_timestamp_from_array($conf['from']);
  $interval = $conf['monthly']['every'];
  $from_month = date("m", $from_timestamp);
  $current_month = date("m", time());
  $difference = $current_month - $from_month;

  // If occurrences is set, check if the number has elapsed.
  if (panel_pane_scheduler_recurring_duration_occurence_expired($difference, $conf)) {
    return FALSE;
  }

  if ($difference % $interval == 0) {
    return ($conf['monthly']['day'] == date('j', time()));
  }
}

/**
 * Pane access check for yearly pattern.
 */
function panel_pane_scheduler_recurring_yearly($conf, $context) {
  $today = time();
  $from_timestamp = panel_pane_scheduler_timestamp_from_array($conf['from']);
  $interval = $conf['yearly']['every'];
  $from_year = date("Y", $from_timestamp);
  $current_year = date("Y", $today);
  $difference = $current_year - $from_year;

  // If occurrences is set, check if the number has elapsed.
  if (panel_pane_scheduler_recurring_duration_occurence_expired($difference, $conf)) {
    return FALSE;
  }

  if ($difference % $interval == 0 || $difference == 0) {
    $month = $conf['yearly']['month'];
    $day = $conf['yearly']['day'];
    return ($month == date('m', $today) && $day == date('j', $today));
  }
}

/**
 * Determines if occurrences exceed the pane duration value.
 *
 * @param int $difference
 *   The of days, weeks, months or years difference between the current date
 *   and the display from date.
 * @param array $conf
 *   The the pane access settings values.
 *
 * @return bool
 *   True if the number of occurrences exceeds the settings, otherwise return
 *   false.
 */
function panel_pane_scheduler_recurring_duration_occurence_expired($difference, array $conf) {
  if (isset($conf['duration']['occurrences'])) {
    return ($difference / $conf[$conf['pattern']]['every']) >= $conf['duration']['occurrences'];
  }
}
