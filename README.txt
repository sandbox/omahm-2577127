INTRODUCTION
------------	

Panel Pane Scheduler provides 2 access plugins for scheduling the
display of Panels Panes.
The first plugin gives the user the option to display or hide a Panels pane
if the current date falls between a 'Display from' and 'Display until' date and
time range.
The second plugin lets the user setup a recurring schedule with the options of
daily, weekly, monthly and yearly patterns and this can run forever, a number of
times or until a certain date.

For a full description of the module, visit the project page:
  http://drupal.org/project/panel_pane_scheduler

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/panel_pane_scheduler


REQUIREMENTS
------------

This module requires the following modules:

* Chaos tools (ctools) - https://www.drupal.org/project/ctools
* Panels - https://www.drupal.org/project/panels


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

* Click on the settings cog of a Panels pane.
* Under 'Visibility rules' click on 'Add new rule'.
* From the 'Add visibility rule' form select the 'Date time schedule' radio
  option and click 'Next'.
* Select a date time range for both 'Display from' and 'Display until' form
  fields.
* To hide the pane within the date range, check the 'Reverse (NOT)' option. It
  will be visible outside of this range.
* Click on the settings cog of a Panels pane.
* Under 'Visibility rules' click on 'Add new rule'.
* From the 'Add visibility rule' form select the 'Date time recurring schedule'
  radio option and click 'Next'.
* Select a date from value, recurring duration option and pattern option.


MAINTAINERS
-----------

Current maintainers:
 * Steve Cousins (omahm) - https://drupal.org/user/540058

ROADMAP
-------

* Visual indication within Panels admin of panes with schedules assigned.
